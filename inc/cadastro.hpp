#include <string>
#ifndef CADASTRO_HPP
#define CADASTRO_HPP

using namespace std;

class Cadastro{
public:
    Cadastro(); //Construtor
    //Cadastro(string nome, long int cpf, long int telefone, string email); //Sobrecarga dos clientes
    //Cadastro(string nome_do_produto, float preco, int quantidade); //Sobrecarga das compras 1
    //Cadastro(int quant, float total); //Sobrecarga das compras 2
    ~Cadastro(); //Destrutor

    //Funções do cliente no modo venda
    void set_nome(string nome);
    string get_nome();
    void set_cpf(long int cpf);
    long int get_cpf();
    void set_telefone(long int telefone);
    long int get_telefone();

    //Funções da compra no modo venda
    void set_nome_do_produto(string nome_do_produto);
    string get_nome_do_produto();
    void set_preco(float preco);
    float get_preco();
    void set_quantidade(int quantidade);
    int get_quantidade();
    void set_total(float total);
    float get_total();
    void set_quant(int quant);
    int get_quant();

    //Funções principais do modo venda
    void modo_venda();
    void consumidores();
    void compras();
    //Arquivos
    void leitura_dados_clientes();
    void imprime_dados_clientes();
    void leitura_dados_compras();
    void imprime_dados_compras();
    void leitura_final_compras();
    void impressao_final_compras();

private:
    string nome;
    long int cpf;
    long int telefone;
    
    string nome_do_produto;
    float preco;
    int quantidade, quant;
    float total;
};

#endif
