#include <string>
#include <iostream>
#include <stdlib.h>
#include <fstream>
//#include <vector>
#include "cadastro.hpp"

using namespace std;

string getString(){
    while(true){
        string valor;
        try{
            getline(cin, valor);    
        }
        catch(const ios_base::failure& e){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida - " << e.what() << " - Insira novamente: " << endl;
        }
        return valor;
    }
}
float get_InputPreco(){
    float preco;
    cin >> preco;
    while(preco <= 0.0){
        cin.clear();
        cin.ignore(32767, '\n');
        cout << "\nVocê digitou um preço inválido ou nulo." << endl;
        cout << "Digite novamente: R$ ";
        cin >> preco;
    }
    return preco;
}
int get_InputQuantidade(){
    int quantidade;
    cin >> quantidade;
    while(quantidade <= 0.0){
        cin.clear();
        cin.ignore(32767, '\n');
        cout << "\nVocê digitou uma quantidade inválida ou nula." << endl;
        cout << "Digite novamente: ";
        cin >> quantidade;
    }
    return quantidade;
}
template <typename T2>
T2 getInput2(){
    while(true){
        T2 valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Opção inválida! Insira novamente: ";
        }else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}

Cadastro::Cadastro(){
    set_nome("");
    set_cpf(0);
    set_telefone(0);

    set_nome_do_produto("");
    set_preco(0.0);
    set_quantidade(0);
    set_total(0.0);
    set_quant(0);
    set_total(0);
}
/*Cadastro::Cadastro(string nome, long int cpf, long int telefone, string email){ //Sobrecarga dos clientes
    set_nome(nome);
    set_cpf(cpf);
    set_telefone(telefone);
    set_email(email);
}
Cadastro::Cadastro(string nome_do_produto, float preco, int quantidade){ //Sobrecarga das compras 1
    set_nome_do_produto(nome_do_produto);
    set_preco(preco);
    set_quantidade(quantidade);
}
Cadastro::Cadastro(int quant, float total){ //Sobrecarga das compras 2
    set_quant(quant);
    set_total(total);
}*/
Cadastro::~Cadastro(){
    //cout << "Destrutor da classe" << endl;
}
void Cadastro::modo_venda(){
    int opcao;

    do{
        system("clear");
        cout << "Você está no modo venda.\n\n";
        cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
        cout << "[1] Cadastrar cliente" << endl;
        cout << "[2] Ir para o carrinho (compras)" << endl;
        cout << "[3] Imprimir dados" << endl;
        cout << "[0] Voltar para a tela inicial" << endl;
        cout << "Escolha o modo que você quer operar[1, 2, 3 ou 0]: ";
        opcao = getInput2<int>();

        switch(opcao){
        case 0:
            cout << "\nVocê optou por voltar à tela inicial." << endl;
            cout << "Pressione Enter para continuar...";
            getchar();
            break;
        case 1:
            consumidores();
            break;
        case 2:
            compras();
            break;
        case 3:
            system("clear");
            imprime_dados_clientes();
            cout << "\n\n";
            imprime_dados_compras();
            cout << "\n";
            impressao_final_compras();
            cout << "Pressione Enter para continuar...";
            getchar();
            break;
        default:
            cout << "\nVocê digitou um número inválido. Digite novamente." << endl;
            cout << "Pressione Enter para continuar...";
            getchar();
        }
    }while(opcao != 0);
}
//Cadastrar clientes
void Cadastro::consumidores(){
    string nome;
    long int telefone, cpf;
    //vector<Cadastro *> clientes;

    system("clear");
    cout << "--------------------------------------" << endl;
    cout << "CADASTRO DO CLIENTE:" << endl;
    cout << "--------------------------------------" << endl;

    cout << "Nome completo: ";
    nome = getString();
    set_nome(nome);

    cout << "CPF (apenas números): ";
    cpf = getInput2<long int>();
    set_cpf(cpf);

    cout << "Telefone para contato (apenas números): ";
    telefone = getInput2<long int>();
    set_telefone(telefone);

    //clientes.push_back(new Cadastro(nome, cpf, telefone, email));

    leitura_dados_clientes();

    cout << "\nCadastro realizado com sucesso." << endl;
    cout << "Pressione Enter para continuar...";
    getchar();
}
//Ir para o carrinho (compras)
void Cadastro::compras(){
    string nome_do_produto;
    float preco;
    int quantidade;
    char escolha;
    float total = 0.0;
    int quant = 0;
    //vector<Cadastro *> compras;

    system("clear");
    cout << "------------------" << endl;
    cout << "CARRINHO:" << endl;
    cout << "------------------" << endl;
    do{
        cout << "Nome do produto: ";
        nome_do_produto = getString();
        set_nome_do_produto(nome_do_produto);

        cout << "Preço: R$ ";
        preco = get_InputPreco();
        set_preco(preco);

        cout << "Quantidade: ";
        quantidade = get_InputQuantidade();
        set_quantidade(quantidade);

        quant = quant+quantidade;
        total = total + (preco*quantidade);

        //compras.push_back(new Cadastro(nome_do_produto, preco, quantidade));

        leitura_dados_compras();

        cout << "\nDeseja cadastrar outro produto?[S/N]: ";
        escolha = getInput2<char>();

        while(escolha != 'S' && escolha != 's' && escolha != 'n' && escolha != 'N'){
            cout << "Opção inválida! Insira novamente: ";
            escolha = getInput2<char>();
        }

        if(escolha == 'n' || escolha == 'N'){
            set_quant(quant);
            set_total(total);
            //compras.push_back(new Cadastro(quant, total));
            leitura_final_compras();

            cout << "\nCompra finalizada." << endl;
            impressao_final_compras();

            cout << "\nO cliente é sócio?[S/N]: ";
            escolha = getInput2<char>();

            if(escolha != 'S' && escolha != 's' && escolha != 'n' && escolha != 'N'){
                while(escolha != 'S' && escolha != 's' && escolha != 'n' && escolha != 'N'){
                    cout << "Opção inválida! Insira novamente: ";
                    escolha = getInput2<char>();
                }
            }else if(escolha == 's' || escolha == 'S'){
                total = get_total()-(get_total()*(15/100));
                set_total(total);
                leitura_final_compras();
                cout << "\nCompra com 15% de desconto:\n\n";
                impressao_final_compras();
            }else if(escolha == 'n' || escolha == 'N'){
                cout << "Compra sem desconto:" << endl;
                impressao_final_compras();
            }

            cout << "\nPressione Enter para continuar...";
            getchar();
            break;
        }
        
        cout << "\n\n";
    }while(escolha == 'S' || escolha == 's');
}
//Métodos do cadastro de clientes
void Cadastro::set_nome(string nome){
    this->nome = nome;
}
string Cadastro::get_nome(){
    return nome;
}
void Cadastro::set_cpf(long int cpf){
    this->cpf = cpf;
}
long int Cadastro::get_cpf(){
    return cpf;
}
void Cadastro::set_telefone(long int telefone){
    this->telefone = telefone;
}
long int Cadastro::get_telefone(){
    return telefone;
}

//Métodos da compra (carrinho)
void Cadastro::set_nome_do_produto(string nome_do_produto){
    this->nome_do_produto = nome_do_produto;
}
string Cadastro::get_nome_do_produto(){
    return nome_do_produto;
}
void Cadastro::set_preco(float preco){
    this->preco = preco;
}
float Cadastro::get_preco(){
    return preco;
}
void Cadastro::set_quantidade(int quantidade){
    this->quantidade = quantidade;
}
int Cadastro::get_quantidade(){
    return quantidade;
}
void Cadastro::set_quant(int quant){
    this->quant = quant;
}
int Cadastro::get_quant(){
    return quant;
}
void Cadastro::set_total(float total){
    this->total = total;
}
float Cadastro::get_total(){
    return total;
}

//Arquivos - clientes
void Cadastro::leitura_dados_clientes(){
    ofstream arquivo_cadastro;

    arquivo_cadastro.open("arquivo_de_cadastro_do_cliente.txt",ios::app);
    if(arquivo_cadastro.is_open()){
        arquivo_cadastro << "----------------------------------------" << endl;
        arquivo_cadastro << "Nome completo: " << get_nome() << endl;
        arquivo_cadastro << "CPF: " << get_cpf() << endl;
        arquivo_cadastro << "Telefone para contato: " << get_telefone() << endl;
        arquivo_cadastro << "----------------------------------------" << endl;
        arquivo_cadastro.close();
    }
}
void Cadastro::imprime_dados_clientes(){
    ifstream arquivo_saida;
    string linha;

    arquivo_saida.open("arquivo_de_cadastro_do_cliente.txt",ios::app);
    if(arquivo_saida.is_open()){
        while(getline(arquivo_saida, linha)){
            cout << linha << endl;
        }
        arquivo_saida.close();
    }
}

//Arquivos - compras
void Cadastro::leitura_dados_compras(){
    ofstream arquivo_dados_compras;
    string x;

    arquivo_dados_compras.open("arquivo_de_dados_compras.txt", ios::app);
    if(arquivo_dados_compras.is_open()){
        arquivo_dados_compras << "--------------------" << endl;
        arquivo_dados_compras << "CARRINHO:" << endl;
        arquivo_dados_compras << "--------------------" << endl;
        arquivo_dados_compras << "Nome do produto: " << get_nome_do_produto() << endl;
        arquivo_dados_compras << "Preço: R$ " << get_preco() << endl;
        arquivo_dados_compras << "Quantidade: " << get_quantidade() << endl;
        arquivo_dados_compras << "--------------------\n\n";
        arquivo_dados_compras.close();
    }
}
void Cadastro::leitura_final_compras(){
    ofstream arquivo_final_compras;

    arquivo_final_compras.open("arquivo_final_compras.txt", ios::out);
    if(arquivo_final_compras.is_open()){
        arquivo_final_compras << "---------------------------------------" << endl;
        arquivo_final_compras << "Quantidade de produtos comprados: " << get_quant() << endl;
        arquivo_final_compras << "Total: R$ " << get_total() << endl;
        arquivo_final_compras << "---------------------------------------" << endl;
        arquivo_final_compras.close();
    }
}
void Cadastro::imprime_dados_compras(){
    ifstream arquivo_saida;
    string linha;

    arquivo_saida.open("arquivo_de_dados_compras.txt", ios::app);
    if(arquivo_saida.is_open()){
        while(getline(arquivo_saida, linha)){
            cout << linha << endl;
        }
        arquivo_saida.close();
    }
}
void Cadastro::impressao_final_compras(){
    ifstream arquivo_saida;
    string linha;

    arquivo_saida.open("arquivo_final_compras.txt", ios::in);
    if(arquivo_saida.is_open()){
        while(getline(arquivo_saida, linha)){
            cout << linha << endl;
        }
        arquivo_saida.close();
    }
}