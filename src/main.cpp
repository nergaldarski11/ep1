#include <iostream>
#include <string>
//#include <vector>
#include <stdlib.h>
#include "cadastro.hpp"

using namespace std;

template <typename T1>

T1 getInput1(){
    while(true){
        T1 valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Opção inválida! Insira novamente: ";
        }else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}

int main()
{
    setlocale(LC_ALL,"portuguese");

    int opcao;
    //vector<Cadastro *> clientes;
    int contador_do_cliente = 0;
    Cadastro clientes[10];


    do{
        system("clear");
        cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
        cout << "Software elaborado em C++\n\n";
        cout << "[1] Modo venda" << endl;
        cout << "[2] Modo recomendação" << endl;
        cout << "[3] Modo estoque" << endl;
        cout << "[0] Sair" << endl;
        cout << "Escolha o modo que você quer operar[1, 2, 3 ou 0]: ";
        opcao = getInput1<int>();

        switch(opcao){
        case 0:
            system("clear");
            cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << endl;
            cout << "Você saiu do sistema." << endl;
            cout << "Para voltar a executá-lo, digite o comando 'make run' no terminal." << endl;
            cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << endl;
            break;
        case 1:
            clientes[contador_do_cliente].modo_venda();
            contador_do_cliente++;
            break;
        case 2:
            break;
        case 3:
            break;
        default:
            cout << "\nVocê digitou um número inválido. Digite novamente." << endl;
            cout << "Pressione Enter para continuar...";
            getchar();
        }
    }while(opcao != 0);

    return 0;
}
